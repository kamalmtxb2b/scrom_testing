export const environment = {
    production: false,
    environment: 'dev',
    scormBaseUrl: 'dist/scorm-player' // need to specify base url of scorm folder
};
  