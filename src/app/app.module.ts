import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgScormPlayerModule } from 'ng-scorm-player';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([]),
    NgScormPlayerModule.forChild({})    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
