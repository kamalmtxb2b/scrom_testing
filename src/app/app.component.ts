import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgScormPlayerService } from 'ng-scorm-player';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit { 
  loadPlayer: boolean = false;
  baseDirectory = "static/";
  // baseDirectory = "https://storage.googleapis.com/maverick-lms-scorm-poc/";
  scormModuleDir = "/brady-survey"; // environment.scormBaseUrl;   
  suspendData: any;
  key = "cmi.suspend_data";

  constructor(private player: NgScormPlayerService, private activtedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.sendMessage({eventName: 'MediatorInitiated'});
    this.setPlayerEvent();
    this.addEventListener();
    this.activtedRoute.queryParams.subscribe(params => {
      if(params.module){
        this.initializeSeedSCORM(params.module);
      }
    })
  }
  
  setPlayerEvent() {
    this.player.initializeEvent.subscribe(val => {
      if (this.suspendData) {
        this.player.SetValue(this.key, this.suspendData);        
      }
      this.sendMessage({ eventName: "LMSInitialize", data: val.runtimeData });
    });
    this.player.setValueEvent.subscribe(val => {
      this.sendMessage({ eventName: "LMSSetValue", data: val.runtimeData });
    })
    this.player.finishEvent.subscribe(val => { 
      this.sendMessage({ eventName: "LMSFinish", data: val.runtimeData });
    }); 
    this.player.commitEvent.subscribe(val => { 
      this.sendMessage({ eventName: "LMSCommit", data: val.runtimeData });
    });
  }

  sendMessage(data) {
    console.log('data from scorm player', data.data);
    window.top.postMessage(data, '*');
  }

  addEventListener() {
    window.addEventListener('message', e => {
      if (e.data.eventName === 'LoadSCORM') {
        if (e.data.data.module) {
          this.scormModuleDir =  this.baseDirectory + e.data.data.module;
          if (e.data.data.suspendData) {
            this.suspendData = e.data.data.suspendData;
          }
          this.loadPlayer = true; 
        }
      }    
    });
  }

  initializeSeedSCORM(moduleName) {
    this.scormModuleDir  = this.baseDirectory + moduleName;
    this.loadPlayer = true; 
  }
}
