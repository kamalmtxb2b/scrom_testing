const express = require('express');
const path = require('path');

const app = express();
const port = process.env.PORT || 8080;
// const persistanceVolume = process.env.persistanceVolumePath || '/pgpool-cache';
const persistanceVolume = '/static';
// const os = require("os");

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", req.header('origin'));
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Credentials","true");
    next();
});

// Serve only the static files form the dist directory
app.use(express.static(__dirname + '/dist/scorm-renderer'));

app.use('/static', 
function(req, res) {
    const referrer = req.headers.referer;
    // req.originalUrl
    // console.log('referrer',referrer);
    // console.log('req.originalUrl',req.originalUrl);
    if(referrer){
        let url = req.originalUrl;
        url = url.replace('/static', '');
        console.log('url = ' + persistanceVolume + url);
        // res.sendFile(persistanceVolume + url);
        
        res.sendFile(path.join(__dirname + persistanceVolume + url));
    }else{
        res.end('access denied');
    }
})


app.get('/*', function(req, res) {
    res.sendFile(path.join(__dirname + '/dist/scorm-renderer/index.html'));
});

// Start the app by listening on the default Heroku port
app.listen(port, function() {
    console.log('Server started successfully on PORT:', port);
});